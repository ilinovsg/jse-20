package ru.ilinovsg.tm.abstractClass;

public class Circle extends Shape {
    public Circle(String name, double area) {
        super(name, area);
    }
}

package ru.ilinovsg.tm.abstractClass;

public abstract class Shape {
    private String name;
    private double area;

    public Shape(String name, double area) {
        this.name = name;
        this.area = area;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getArea() {
        return area;
    }
}

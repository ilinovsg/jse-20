package ru.ilinovsg.tm.abstractClass;

public class Square extends Shape {
    public Square(String name, double area) {
        super(name, area);
    }
}

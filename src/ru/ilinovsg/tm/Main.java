package ru.ilinovsg.tm;

import ru.ilinovsg.tm.abstractClass.Circle;
import ru.ilinovsg.tm.abstractClass.Rectangle;
import ru.ilinovsg.tm.abstractClass.Shape;
import ru.ilinovsg.tm.abstractClass.Square;

import java.util.ArrayList;
import java.util.Collection;

public class Main {


    public static void main(String[] args) {

        Collection<Shape> shapes = new ArrayList();
        initCollection(shapes, new Circle("Shape1", 1));
        initCollection(shapes, new Rectangle("Shape2", 2));
        initCollection(shapes, new Square("Shape3", 5));

        Collection<Circle> circles = new ArrayList();
        initCollection(circles, new  Circle("Circle1", 10));
        initCollection(circles, new  Circle("Circle2", 50));

        Collection<Rectangle> rectangles = new ArrayList();
        initCollection(rectangles, new  Rectangle("Rectangles1", 100));
        initCollection(rectangles, new  Rectangle("Rectangles2", 200));

        Collection<Square> squares = new ArrayList();
        initCollection(squares, new  Square("Rectangles1", 3));
        initCollection(squares, new  Square("Rectangles2", 15));

        executeArea(shapes);
        executeArea(circles);
        executeArea(rectangles);
        executeArea(squares);
    }

    private static <T> T initCollection(Collection<T> shapes, T element){
        shapes.add(element);
        return element;
    }

    private static void executeArea(Collection<? extends Shape> shapes){
        double area = 0.0;
        for (Shape shape : shapes){
            area += shape.getArea();
        }
        System.out.println(area);
    }

}
